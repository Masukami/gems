import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SharedModule } from './shared/shared.module';
import { HeaderComponent } from './header/header.component';

// Survey Components
import { SurveyComponent } from './survey/survey.component';

/* Routing Module */
import { AppRoutingModule } from './app-routing.module';
import { CompletemodalComponent } from './completemodal/completemodal.component';
import { HttpModule } from '../../node_modules/@angular/http';
import { ResultComponent } from './result/result.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HeaderComponent,
    SurveyComponent,
    CompletemodalComponent,
    ResultComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    HttpModule
  ],
  entryComponents: [CompletemodalComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
