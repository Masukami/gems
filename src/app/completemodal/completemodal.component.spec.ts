import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletemodalComponent } from './completemodal.component';

describe('CompletemodalComponent', () => {
  let component: CompletemodalComponent;
  let fixture: ComponentFixture<CompletemodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletemodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletemodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
