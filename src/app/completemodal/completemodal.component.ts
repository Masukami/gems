import { Component } from '@angular/core';
import { MzBaseModal } from 'ngx-materialize';

@Component({
  selector: 'app-completemodal',
  templateUrl: './completemodal.component.html',
  styleUrls: ['./completemodal.component.css']
})
export class CompletemodalComponent extends MzBaseModal {

}
