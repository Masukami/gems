import { Component, OnInit, ViewChild, Renderer } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from '@angular/forms';
import { SurveyModule } from '../models/form-data.model';
import {
  MzCollapsibleComponent,
  MzToastService,
  MzModalService
} from '../../../node_modules/ngx-materialize';
import { SurveyService } from '../survey.service';
import { CompletemodalComponent } from 'src/app/completemodal/completemodal.component';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})
export class SurveyComponent implements OnInit {
  behaviorForm: FormGroup;
  normativeForm: FormGroup;
  controlForm: FormGroup;
  attitudeForm: FormGroup;
  subjectiveForm: FormGroup;
  perceivedForm: FormGroup;
  intentionForm: FormGroup;
  organization: FormControl;

  behaviorSSubmit = false;
  normativeSSubmit = false;
  controlSSubmit = false;
  attitudeSSubmit = false;
  subjectiveSSubmit = false;
  perceivedSSubmit = false;
  intentionSSubmit = false;
  surveySSubmit = false;

  surveyResult: SurveyModule[];

  @ViewChild('collapsibleController')
  collapsibleController: MzCollapsibleComponent;

  constructor(
    private surveyServ: SurveyService,
    private toastServ: MzToastService,
    private fb: FormBuilder,
    private modalService: MzModalService,
    private renderer: Renderer
  ) {
    this.initializeFormGroup();
  }

  initializeFormGroup() {
    this.organization = new FormControl(null, Validators.required);
    this.behaviorForm = new FormGroup({
      bb1: new FormControl(null, Validators.required),
      bb2: new FormControl(null, Validators.required),
      bb3: new FormControl(null, Validators.required),
      bb4: new FormControl(null, Validators.required),
      bb5: new FormControl(null, Validators.required),
      bb6: new FormControl(null, Validators.required),
      bb7: new FormControl(null, Validators.required)
    });
    this.normativeForm = new FormGroup({
      nb1: new FormControl(null, [Validators.required]),
      nb2: new FormControl(null, [Validators.required]),
      nb3: new FormControl(null, [Validators.required])
    });
    this.controlForm = new FormGroup({
      cb1: new FormControl(null, [Validators.required]),
      cb2: new FormControl(null, [Validators.required]),
      cb3: new FormControl(null, [Validators.required]),
      cb4: new FormControl(null, [Validators.required])
    });
    this.attitudeForm = new FormGroup({
      at1: new FormControl(null, [Validators.required]),
      at2: new FormControl(null, [Validators.required]),
      at3: new FormControl(null, [Validators.required]),
      at4: new FormControl(null, [Validators.required]),
      at5: new FormControl(null, [Validators.required]),
      at6: new FormControl(null, [Validators.required]),
      at7: new FormControl(null, [Validators.required])
    });
    this.subjectiveForm = new FormGroup({
      sn1: new FormControl(null, [Validators.required]),
      sn2: new FormControl(null, [Validators.required]),
      sn3: new FormControl(null, [Validators.required])
    });
    this.perceivedForm = new FormGroup({
      pbc1: new FormControl(null, [Validators.required]),
      pbc2: new FormControl(null, [Validators.required]),
      pbc3: new FormControl(null, [Validators.required])
    });
    this.intentionForm = new FormGroup({
      is1: new FormControl(null, [Validators.required]),
      is2: new FormControl(null, [Validators.required]),
      is3: new FormControl(null, [Validators.required])
    });
  }

  behavioralSubmit() {
    this.behaviorSSubmit = true;
    if (this.behaviorForm.valid) {
      this.collapsibleController.close(0);
      this.collapsibleController.open(1);
    } else {
      this.toastServ.show(
        'Please answer all Behavioral Beliefs questions!',
        3000,
        'green'
      );
    }
  }

  normativeSubmit() {
    this.normativeSSubmit = true;
    if (this.normativeForm.valid) {
      this.collapsibleController.close(1);
      this.collapsibleController.open(2);
    } else {
      this.toastServ.show(
        'Please answer all Normative Beliefs questions!',
        3000,
        'green'
      );
    }
  }

  controlSubmit() {
    this.controlSSubmit = true;
    if (this.controlForm.valid) {
      this.collapsibleController.close(2);
      this.collapsibleController.open(3);
    } else {
      this.toastServ.show(
        'Please answer all Control Beliefs questions!',
        3000,
        'green'
      );
    }
  }

  attitudeSubmit() {
    this.attitudeSSubmit = true;
    if (this.attitudeForm.valid) {
      this.collapsibleController.close(3);
      this.collapsibleController.open(4);
    } else {
      this.toastServ.show(
        'Please answer all Attitude questions!',
        3000,
        'green'
      );
    }
  }

  subjectiveSubmit() {
    this.subjectiveSSubmit = true;
    if (this.subjectiveForm.valid) {
      this.collapsibleController.close(4);
      this.collapsibleController.open(5);
    } else {
      this.toastServ.show(
        'Please answer all Subjective Norm questions!',
        3000,
        'green'
      );
    }
  }

  perceivedSubmit() {
    this.perceivedSSubmit = true;
    if (this.perceivedForm.valid) {
      this.collapsibleController.close(5);
      this.collapsibleController.open(6);
    } else {
      this.toastServ.show(
        'Please answer all Perceived Behavioral Control questions!',
        3000,
        'green'
      );
    }
  }

  intentionSubmit() {
    this.intentionSSubmit = true;
    if (this.intentionForm.valid) {
      this.collapsibleController.close(6);
    } else {
      this.toastServ.show(
        'Please answer all Intentions to Attend Green Events questions!',
        3000,
        'green'
      );
    }
  }

  surveyComplete() {
    if (this.organization.valid) {
      const surveyResult = this.surveyServ.EvaluateSurvey(
        this.behaviorForm,
        this.normativeForm,
        this.controlForm,
        this.attitudeForm,
        this.subjectiveForm,
        this.perceivedForm,
        this.intentionForm,
        this.organization.value
      );
      this.surveyServ.saveResult(surveyResult)
      .subscribe(result => {
        console.log('Save result : ' + result);
      });
      this.resetForm();
      this.modalService.open(CompletemodalComponent);
      console.log(surveyResult);
    } else {
      this.toastServ.show('Please enter your organization', 3000, 'green');
    }
  }

  // Get Results
  // this.surveyServ.getResults().subscribe(results => {
  //   result;
  // });

  resetForm() {
    this.behaviorForm.reset();
    this.normativeForm.reset();
    this.controlForm.reset();
    this.attitudeForm.reset();
    this.subjectiveForm.reset();
    this.perceivedForm.reset();
    this.intentionForm.reset();
    this.organization.reset();
    this.behaviorSSubmit = false;
    this.normativeSSubmit = false;
    this.controlSSubmit = false;
    this.attitudeSSubmit = false;
    this.subjectiveSSubmit = false;
    this.perceivedSSubmit = false;
    this.intentionSSubmit = false;
    this.surveySSubmit = false;
  }

  ngOnInit() {
    const scrollSpy = $('.scrollspy');
    this.renderer.invokeElementMethod(scrollSpy, 'scrollSpy');
  }

  behaviorField(field) {
    return this.behaviorForm.get(field);
  }

  normativeField(field) {
    return this.normativeForm.get(field);
  }

  controlField(field) {
    return this.controlForm.get(field);
  }

  attitudeField(field) {
    return this.attitudeForm.get(field);
  }

  subjectiveField(field) {
    return this.subjectiveForm.get(field);
  }

  perceivedField(field) {
    return this.perceivedForm.get(field);
  }

  intentionField(field) {
    return this.intentionForm.get(field);
  }

  getErrorMsg() {
    return 'Please tick your choice';
  }

  get surveyStatus() {
    if (
      this.behaviorForm.valid &&
      this.normativeForm.valid &&
      this.controlForm.valid &&
      this.attitudeForm.valid &&
      this.subjectiveForm.valid &&
      this.perceivedForm.valid &&
      this.intentionForm.valid
    ) {
      return true;
    } else {
      return false;
    }
  }
}
