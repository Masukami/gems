import { Component, OnInit } from '@angular/core';
import { SurveyService } from '../survey.service';
import { SurveyModule } from '../models/form-data.model';
import { FormControl, Validators, FormGroup } from '../../../node_modules/@angular/forms';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  password: FormControl;
  accessSubmit = false;
  showResult = false;
  results = null;

  constructor(private surveyServ: SurveyService) {
    this.password = new FormControl(null, Validators.required);
  }

  getAccess() {
    this.accessSubmit = true;
    if (this.password.valid) {
      if (this.password.value === 'gems') {
        this.showResult = true;
        this.surveyServ.getResults().subscribe(result => {
          this.results = result;
        });
      }
    }
  }

  get resultsData() {
    console.log(this.results);
    return this.results;
  }

  get passwordControl() {
    return this.password;
  }

  ngOnInit() {}

}
