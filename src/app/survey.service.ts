import { Injectable } from '@angular/core';
import { SurveyModule, Questions } from './models/form-data.model';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class SurveyService {
  surveyResult: SurveyModule;
  constructor(private http: Http) {}

  saveResult(surveyResult) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/results', JSON.stringify(surveyResult), {headers: headers})
    .map(res => res.json());
  }

  getResults() {
    return this.http
      .get('http://localhost:3000/results')
      .map(res => {
          return res.json();
        }
      );
  }

  EvaluateSurvey(
    behavior,
    normative,
    control,
    attitude,
    subjective,
    perceived,
    intention,
    organization
  ) {
    let behaviorScore = 0,
      normativeScore = 0,
      controlScore = 0,
      attitudeScore = 0,
      subjectiveScore = 0,
      perceivedScore = 0,
      intentionScore = 0;

    Object.keys(behavior.controls).forEach(obj => {
      behaviorScore += parseInt(behavior.get(obj).value, 10);
    });

    Object.keys(normative.controls).forEach(obj => {
      normativeScore += parseInt(normative.get(obj).value, 10);
    });

    Object.keys(control.controls).forEach(obj => {
      controlScore += parseInt(control.get(obj).value, 10);
    });

    Object.keys(attitude.controls).forEach(obj => {
      attitudeScore += parseInt(attitude.get(obj).value, 10);
    });

    Object.keys(subjective.controls).forEach(obj => {
      subjectiveScore += parseInt(subjective.get(obj).value, 10);
    });

    Object.keys(perceived.controls).forEach(obj => {
      perceivedScore += parseInt(perceived.get(obj).value, 10);
    });

    Object.keys(intention.controls).forEach(obj => {
      intentionScore += parseInt(intention.get(obj).value, 10);
    });

    const behaviorQuestion = new Questions('Behavioral Beliefs', behaviorScore);
    const normativeQuestion = new Questions(
      'Normative Beliefs',
      normativeScore
    );
    const controlQuestion = new Questions('Control Beliefs', controlScore);
    const attitudeQuestion = new Questions('Attitude', attitudeScore);
    const subjectiveQuestion = new Questions(
      'Subjective Norm',
      subjectiveScore
    );
    const perceivedQuestion = new Questions(
      'Perceived Behavioral Control',
      perceivedScore
    );
    const intentionQuestion = new Questions(
      'Intentions to Attend Green Events',
      intentionScore
    );
    const totalScore =
      behaviorScore +
      normativeScore +
      controlScore +
      attitudeScore +
      subjectiveScore +
      perceivedScore +
      intentionScore;

    let result = '';

    if (totalScore > 0 && totalScore < 32) {
      result = 'Very weak intention to green events';
    } else if (totalScore > 31 && totalScore < 63) {
      result = 'Weak intention to green events';
    } else if (totalScore > 62 && totalScore < 94) {
      result = 'Moderate intention to green events';
    } else if (totalScore > 93 && totalScore < 125) {
      result = 'Strong intention to green events';
    } else if (totalScore > 124 && totalScore < 155) {
      result = 'Very strong intention to green events';
    }

    this.surveyResult = new SurveyModule(
      [
        behaviorQuestion,
        normativeQuestion,
        controlQuestion,
        attitudeQuestion,
        subjectiveQuestion,
        perceivedQuestion,
        intentionQuestion
      ],
      totalScore,
      organization,
      result
    );

    return this.surveyResult;
  }
}
