import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MzNavbarModule,
  MzParallaxModule,
  MzButtonModule,
  MzIconMdiModule,
  MzInputModule,
  MzTextareaModule,
  MzToastModule,
  MzSpinnerModule,
  MzRadioButtonModule,
  MzCollapsibleModule,
  MzModalModule,
  MzCardModule,
  MzCollectionModule
} from 'ngx-materialize';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    MzNavbarModule,
    MzParallaxModule,
    MzButtonModule,
    MzIconMdiModule,
    MzInputModule,
    MzTextareaModule,
    MzToastModule,
    MzSpinnerModule,
    MzRadioButtonModule,
    MzCollapsibleModule,
    ReactiveFormsModule,
    MzModalModule,
    MzCardModule,
    NgxPaginationModule,
    MzCollectionModule
  ],
  exports: [
    MzNavbarModule,
    MzParallaxModule,
    MzButtonModule,
    MzIconMdiModule,
    MzInputModule,
    MzTextareaModule,
    MzToastModule,
    MzSpinnerModule,
    MzRadioButtonModule,
    MzCollapsibleModule,
    ReactiveFormsModule,
    MzModalModule,
    MzCardModule,
    NgxPaginationModule,
    MzCollectionModule
  ],
  declarations: []
})
export class SharedModule {}
