import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SurveyComponent } from './survey/survey.component';
import { ResultComponent } from './result/result.component';

const surveyRoute: Routes = [
  { path: 'survey', component: SurveyComponent },
  { path: 'result', component: ResultComponent },
  { path: '', redirectTo: '/survey', pathMatch: 'full' }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(surveyRoute, { useHash: true })
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {}
