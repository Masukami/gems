export class Questions {
  questionCode: any;
  questionScore: any;

  constructor(questionCode, questionScore) {
    this.questionCode = questionCode;
    this.questionScore = questionScore;
  }
}

export class SurveyModule {
  questionScore: Questions[];
  totalScore: any;
  organization: any;
  result: any;

  constructor(questionScore, totalScore, organization, result) {
    this.questionScore = questionScore;
    this.totalScore = totalScore;
    this.organization = organization;
    this.result = result;
  }
}
