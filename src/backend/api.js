const express = require("express");
const mongojs = require('mongojs');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}))
app.use((req,res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

const db = mongojs('mongodb://suhail:asd12345@ds018498.mlab.com:18498/gems', ['results']);
const port = 3000;

app.get('/results', (req, res) => {
  db.results.find( (err, result) => {
    if (err) res.send(err);
    res.json(result);
  });
});

app.post('/results', (req, res) => {
  db.results.save(req.body, (err, result) => {
    if (err) res.send(err);
    res.json(result);
  });
});

app.listen(port, () => {
  console.log('Listening to ' + port);
});
